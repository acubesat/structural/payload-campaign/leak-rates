Χρήση ηλίου ως tracer gas, καθώς έχει x3 την ταχύτητα του αέρα -> ταχύτερη αναγνώριση
[Αναλυτική επεξήγηση leak test με He detection](https://cds.cern.ch/record/1047068/files/p227.pdf)

~~Άμα γεμίσουμε το container με He έναντι ατμοσφαιρικού αέρα, μέχρι πίεση 1.2 atm, θα μπορούσαμε να το τρέξουμε έτσι σε leak test?~~

Για να κάνουμε το Helium test method, πρέπει πρώτα να εκκενώσουμε από αέρα το container?
